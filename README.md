# Django Two-Shot 

This project functions like a model banking interface, allowing the user to create and edit accounts
as well as add and edit receipts related to each account.

## Features
* Users can log in and out
* Users can create banking accounts
* Users can add receipts to their accounts

## Getting Started
To run the Project Management Demo App locally, follow these steps:

1. **Clone the Repository:**
   ```bash
   git clone https://gitlab.com/thedylanminton/django-one-shot.git

2. **Navigate to the Project Directory:**
   ```bash
   python manage.py runserver

3. **Open the App in Your Browser:**

   localhost:8000
